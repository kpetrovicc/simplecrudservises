module github.com/mi15110/go-employers

go 1.17

require github.com/gorilla/mux v1.8.0

require (
	github.com/jinzhu/gorm v1.9.16
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)

require (
	github.com/go-sql-driver/mysql v1.5.0 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/ramya-rao-a/go-outline v0.0.0-20210608161538-9736a4bde949 // indirect
	golang.org/x/tools v0.1.1 // indirect
)
