package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	_ "github.com/jinzhu/gorm/dialects/mysql"

	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/mi15110/go-employers/pkg/app"
	"github.com/mi15110/go-employers/pkg/routes"
	yaml "gopkg.in/yaml.v3"
)

func main() {
	r := mux.NewRouter()
	routes.RegiterEmployers(r)
	http.Handle("/", r)

	// New organization -> yaml config
	connectionFilename := "pkg/config/connection.yaml"
	configFile, err := ioutil.ReadFile(connectionFilename)
	if err != nil {
		log.Println("Cannot read configuration - aborting.", err)
		return // check
	}

	var serveConfig app.Serve
	err = yaml.Unmarshal(configFile, &serveConfig)
	if err != nil {
		log.Println("Cannot parse configuration - aborting.", err)
		return
	}

	// Starts an HTTP server with a given address and handler
	address := fmt.Sprintf("%s:%d", serveConfig.Address, serveConfig.Port)
	fmt.Println("Active port listening: ", serveConfig.Port)
	log.Fatal(http.ListenAndServe(address, r))
}
