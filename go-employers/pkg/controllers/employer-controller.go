package controllers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"github.com/mi15110/go-employers/pkg/models"
	"github.com/mi15110/go-employers/pkg/utils"
)

var newEmployer models.Employer

func GetEmployers(w http.ResponseWriter, r *http.Request) {
	newEmployer := models.GetEmployers()
	res, _ := json.Marshal(newEmployer)
	w.Header().Set("Content-Type", "aplication/json")
	w.WriteHeader(http.StatusOK)
	w.Write(res)
}

func GetEmployerById(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	employerId := vars["id"]
	id, err := strconv.ParseInt(employerId, 0, 0) // Parse to int
	if err != nil {
		fmt.Println("error while parsing")
	}

	employerInfo, _ := models.GetEmployerById(id)
	res, _ := json.Marshal(employerInfo)
	w.Header().Set("Content-Type", "pkglication/json")
	w.WriteHeader(http.StatusOK)
	w.Write(res)
}

// Creating a new employer
func CreateEmployer(w http.ResponseWriter, r *http.Request) {
	CreateEmployer := &models.Employer{}
	utils.ParseBody(r, CreateEmployer)

	// // Read request body.
	// body, err := ioutil.ReadAll(r.Body)
	// if err != nil {
	// 	fmt.Println(err.Error())
	// 	w.WriteHeader(http.StatusInternalServerError) // Return 500 Internal Server Error.
	// 	return
	// }

	// // Parse body as json.
	// if err = json.Unmarshal(body, &CreateEmployer); err != nil {
	// 	fmt.Println(err.Error())
	// 	w.WriteHeader(http.StatusBadRequest) // Return 400 Bad Request.
	// 	return
	// }

	e := CreateEmployer.CreateEmployer()
	res, _ := json.Marshal(e)
	w.Header().Set("Content-Type", "pkglication/json")
	w.WriteHeader(http.StatusOK)
	w.Write(res)
}

func DeleteEmployerById(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	employerId := vars["id"]
	id, err := strconv.ParseInt(employerId, 0, 0) // Parse to int.
	if err != nil {
		fmt.Println("error while parsing")
	}

	employer := models.DeleteEmployerById(id)
	res, _ := json.Marshal(employer)
	w.Header().Set("Content-Type", "pkglication/json")
	w.WriteHeader(http.StatusOK)
	w.Write(res)
}

func UpdateEmployerById(w http.ResponseWriter, r *http.Request) {
	var UpdateEmployer = &models.Employer{}

	// Read request body.
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Println(err.Error())
		w.WriteHeader(http.StatusInternalServerError) // Return 500 Internal Server Error.
		return
	}

	// Parse body as json.
	if err = json.Unmarshal(body, &UpdateEmployer); err != nil {
		fmt.Println(err.Error())
		w.WriteHeader(http.StatusBadRequest) // Return 400 Bad Request.
		return
	}

	vars := mux.Vars(r)
	employerId := vars["id"]
	id, err := strconv.ParseInt(employerId, 0, 0)
	if err != nil {
		fmt.Println("error while parsing")
	}

	employerInfo, db := models.GetEmployerById(id)

	if UpdateEmployer.First_name != "" {
		employerInfo.First_name = UpdateEmployer.First_name
	}

	if UpdateEmployer.Last_name != "" {
		employerInfo.Last_name = UpdateEmployer.Last_name
	}

	if UpdateEmployer.Gender != "" {
		employerInfo.Gender = UpdateEmployer.Gender
	}

	if UpdateEmployer.Ip_address != "" {
		employerInfo.Ip_address = UpdateEmployer.Ip_address
	}

	db.Save(&employerInfo)
	res, _ := json.Marshal(employerInfo)
	w.Header().Set("Content-Type", "pkglication/json")
	w.WriteHeader(http.StatusOK)
	w.Write(res)

}
