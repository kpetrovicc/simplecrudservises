package models

import (
	"github.com/jinzhu/gorm"
	"github.com/mi15110/go-employers/pkg/app"
)

var db *gorm.DB

type Employer struct {
	ID         string `json: "id"`
	First_name string `json: "first_name"`
	Last_name  string `json: "last_name"`
	Gender     string `json: "gender"`
	Ip_address string `json: "ip_address"`
}

func init() {
	// Connect to database
	app.Connect()
	db = app.GetDB()
	db.AutoMigrate(&Employer{})
}

// Create new employer
func (e *Employer) CreateEmployer() *Employer {
	db.Table("employers").NewRecord(e)
	db.Table("employers").Create(&e)

	return e
}

// Get all employers from database
func GetEmployers() []Employer {
	var Employers []Employer
	db.Table("employers").Find(&Employers)

	return Employers
}

// Get employer by id
func GetEmployerById(id int64) (Employer, *gorm.DB) {
	var emp Employer
	db := db.Table("employers").Where("ID = ?", id).Find(&emp)
	return emp, db
}

// Delete user
func DeleteEmployerById(id int64) Employer {
	var employer Employer
	db.Table("employers").Where("ID = ?", id).Delete(employer)
	return employer
}
