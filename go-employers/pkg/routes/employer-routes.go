package routes

import (
	"github.com/gorilla/mux"
	"github.com/mi15110/go-employers/pkg/controllers"
)

// All routes and routes handlers
var RegiterEmployers = func(router *mux.Router) {
	router.HandleFunc("/employer/", controllers.CreateEmployer).Methods("POST")
	router.HandleFunc("/employer/", controllers.GetEmployers).Methods("GET")
	router.HandleFunc("/employer/{id}", controllers.GetEmployerById).Methods("GET")
	router.HandleFunc("/employer/{id}", controllers.DeleteEmployerById).Methods("DELETE")
	router.HandleFunc("/employer/{id}", controllers.UpdateEmployerById).Methods("PUT")
}
