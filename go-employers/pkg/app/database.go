package app

type Database struct {
	User     string `yaml:"user"`
	Password string `yaml:"password"`
	Protocol string `yaml:"protocol"`
	Address  string `yaml:"address"`
	Dbname   string `yaml:"dbname"`
}
