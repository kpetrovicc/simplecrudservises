package app

import (
	"fmt"
	"io/ioutil"
	"log"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"gopkg.in/yaml.v3"
)

var (
	db *gorm.DB
)

// Connect to database
func Connect() {

	configFilename := "pkg/config/database.yaml"
	configFile, err := ioutil.ReadFile(configFilename)
	if err != nil {
		log.Println("Cannot read configuration - aborting.", err)
		return
	}

	var databaseConfig Database
	err = yaml.Unmarshal(configFile, &databaseConfig)
	if err != nil {
		log.Println("Cannot parse configuration - aborting.", err)
		return
	}

	// Connect to data base using given parameters
	dsn := fmt.Sprintf("%s:%s@%s(%s)/%s", databaseConfig.User, databaseConfig.Password, databaseConfig.Protocol, databaseConfig.Address, databaseConfig.Dbname)
	fmt.Println(dsn)
	trydb, err := gorm.Open("mysql", dsn)
	if err != nil {
		fmt.Println(err.Error())
		panic("Cannot connect to database.")
	}

	db = trydb
	fmt.Println("Successfully connected to database.")
}

func GetDB() *gorm.DB {
	return db
}
