package app

type Serve struct {
	Port    int    `yaml:"port"`
	Address string `yaml:"address"`
}
